/*
 gradle-jni-multilib
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl-3.0>.
*/

package msrd0.jni.jni_multilib

enum class OS(val s : String, val soPrefix : String, val soSuffix : String)
{
	LINUX("linux", "lib", ".so"),
	WINDOWS("win32", "", ".dll"),
	ANDROID("android", "lib", ".so")
}

enum class Architecture(val s : String, val flags : Array<String>)
{
	X86		("i686", 	arrayOf("-m32", "-march=i686", "-mtune=generic")),
	X86_64	("x86_64", 	arrayOf("-march=x86-64", "-mtune=generic")),
	ARMv7	("armv7", 	arrayOf("-march=armv7-a", "-mtune=generic-armv7-a")),
	AARCH64	("aarch64", arrayOf("-march=armv8-a"))
}

data class Arch(
		val os : OS,
		val arch : Architecture
)
