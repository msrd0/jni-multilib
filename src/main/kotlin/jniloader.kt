/*
 jni-multilib
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl-3.0>.
*/

package msrd0.jni.jni_multilib

import org.apache.commons.io.IOUtils
import org.slf4j.*
import java.io.*
import java.lang.ClassLoader.*
import java.net.URL

object JniLoader
{
	private val logger : Logger = LoggerFactory.getLogger(JniLoader::class.java)
	
	private fun sysprop(name : String) = System.getProperty(name).toLowerCase()
	
	fun withAlternatives(os : OS) : List<OS> = when(os) {
		OS.WINDOWS -> listOf(OS.WINDOWS)
		OS.LINUX   -> listOf(OS.LINUX, OS.ANDROID)
		OS.ANDROID -> listOf(OS.ANDROID, OS.LINUX)
	}
	
	fun withAlternatives(arch : Architecture) : List<Architecture> = when(arch) {
		Architecture.X86     -> listOf(Architecture.X86)
		Architecture.X86_64  -> listOf(Architecture.X86_64, Architecture.X86)
		Architecture.ARMv7   -> listOf(Architecture.ARMv7)
		Architecture.AARCH64 -> listOf(Architecture.AARCH64, Architecture.ARMv7)
	}
	
	fun withAlternatives(a : Arch) : List<Arch>
	{
		val l = ArrayList<Arch>()
		for (os in withAlternatives(a.os))
			for (arch in withAlternatives(a.arch))
				l.add(Arch(os, arch))
		return l
	}
	
	@JvmStatic
	@Throws(IOException::class)
	fun loadLibrary(name : String)
	{
		var os = OS.LINUX
		if (sysprop("os.name").contains("win"))
			os = OS.WINDOWS
		if (sysprop("java.vendor").contains("android"))
			os = OS.ANDROID
		logger.debug("Detected OS: $os")
		
		var arch = Architecture.X86
		if (sysprop("os.arch").contains("64"))
			arch = Architecture.X86_64
		if (sysprop("os.arch").contains("arm"))
			arch = Architecture.ARMv7
		// TODO what does java tell on an aarch64 machine?
		logger.debug("Detected Architecture: $arch")
		
		for (a in withAlternatives(Arch(os, arch)))
		{
			val resName = "lib-${a.arch.s}-${a.os.s}/${a.os.soPrefix}$name${a.os.soSuffix}"
			logger.debug("Looking for $resName")
			val url : URL? = getSystemResource(resName)
			if (url == null)
				continue
			val tmp = File.createTempFile(a.os.soPrefix, a.os.soSuffix)
			tmp.deleteOnExit()
			IOUtils.copy(getSystemResourceAsStream(resName), FileOutputStream(tmp))
			
			System.load(tmp.absolutePath)
			logger.info("Loaded $resName")
			break
		}
	}
}
