# jni-multilib [![GitLab CI Build Status](https://gitlab.com/msrd0/jni-multilib/badges/master/build.svg)](https://gitlab.com/msrd0/jni-multilib/pipelines) [![License](https://img.shields.io/badge/license-LGPL--3.0-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)

A gradle plugin that can compile JNI code for multiple architectures, bundle it with the jar and choose
the right library at runtime.

Take a look at the [example](https://gitlab.com/msrd0/jni-multilib/tree/master/example).

## Gradle

```gradle
buildscript {
	repositories {
		mavenLocal()
		jcenter()
		maven { url "https://maven.nextgenenergy.co.nz/artifactory/maven" }
	}
	dependencies {
		classpath "msrd0.jni:jni-multilib-gradle:+"
	}
}

apply plugin: 'gradle-jni-multilib'

repositories {
	mavenLocal()
	jcenter()
	maven { url "https://maven.nextgenenergy.co.nz/artifactory/maven" }
}

dependencies {
	compile "msrd0.jni:jni-multilib:+"
}
```
