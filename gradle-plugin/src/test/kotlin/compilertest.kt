/*
 gradle-jni-multilib
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl-3.0>.
*/

import msrd0.jni.jni_multilib.gradle.Compilers
import org.hamcrest.MatcherAssert.*
import org.hamcrest.Matchers.*
import org.testng.annotations.Test
import msrd0.jni.jni_multilib.gradle.Compilers.searchCompilers
import msrd0.jni.jni_multilib.gradle.Compilers.compilers

class CompilerTest
{
	@Test
	fun compilerSearch()
	{
		searchCompilers(Compilers.commonCFlags, Compilers.commonCxxFlags, Compilers.commonLdFlags, Compilers.windowsLdFlags, null, null)
		for ((arch, compiler) in compilers)
			println(" -> Compiler ${compiler.cc} of ${compiler.native} for $arch")
		assertThat(compilers.size, greaterThan(0))
	}
}
