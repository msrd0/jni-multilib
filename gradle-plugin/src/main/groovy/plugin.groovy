/*
 gradle-jni-multilib
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl-3.0>.
*/

package msrd0.jni.jni_multilib.gradle

import org.gradle.api.*

/**
 * The main plugin class for `gradle-jni-multilib`. To use it, add this to your `build.gradle`:
 *
 * ```
 * buildscript {
 *     // ...
 *     dependencies {
 *         classpath "msrd0.gradle:gradle-jni-multilib:+"
 *     }
 * }
 *
 * apply plugin: 'gradle-jni-multilib'
 * ```
 */
class JniMultilibPlugin implements Plugin<Project>
{
	@Override
	void apply(Project proj)
	{
		// first apply the java plugin
		proj.configure(proj) {
			apply plugin: 'java'
		}
		
		// then add jni task
		proj.task("jni", type: JniMultilibTask) {
			description = "Create JNI Multilib libraries"
		}
		
		// and finally make the resources depend on jni
		proj.configure(proj) {
			processResources {
				dependsOn "jni"
			}
		}
	}
}
