#include <stdio.h>

int main()
{
#if defined __x86_64__
	printf("x86_64\n");
#elif defined __i686__
	printf("i686\n");
#elif defined __arm__
	printf("armv7\n");
#elif defined __aarch64__
	printf("aarch64\n");
#else
	printf("???\n");
#endif

	return 0;
}
