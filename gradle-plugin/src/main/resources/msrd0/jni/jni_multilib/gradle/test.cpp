#include <iostream>
using namespace std;

int main()
{
	int ret = 0;

#if defined __x86_64__
	cout << "x86_64" << endl;
#elif defined __i686__
	cout << "i686" << endl;
#elif defined __arm__
	cout << "armv7" << endl;
#elif defined __aarch64__
	cout << "aarch64" << endl;
#else
	ret = 1;
#endif

	return ret;
}
