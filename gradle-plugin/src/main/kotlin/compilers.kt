/*
 gradle-jni-multilib
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl-3.0>.
*/

package msrd0.jni.jni_multilib.gradle

import msrd0.jni.jni_multilib.*
import org.apache.commons.io.IOUtils
import java.io.*
import java.lang.ProcessBuilder.Redirect.*
import java.nio.charset.StandardCharsets.*
import java.util.*

object Compilers
{
	val compilers = HashMap<Arch, Compiler>()
	
	val commonCFlags   = arrayOf("-fPIC", "-O2", "-Wall", "-std=c11")
	val commonCxxFlags = arrayOf("-fPIC", "-O2", "-Wall", "-std=c++11")
	val commonLdFlags  = arrayOf("-fPIC", "-shared", "-Wl,-O1,--sort-common,--as-needed,-z,relro")
	val windowsLdFlags = arrayOf("-fPIC", "-shared", "-Wl,-O1,--sort-common,--as-needed")
	
	private fun runAndOutput(vararg command : String) : String?
	{
		try
		{
			val builder = ProcessBuilder(*command)
			logger.debug("$" + builder.command().joinToString(" "))
			builder.redirectError(PIPE)
			val process = builder.start()
			process.waitFor()
			return IOUtils.toString(process.inputStream, UTF_8).trim()
		}
		catch (ex : IOException)
		{
			ex.printStackTrace()
			return null
		}
	}
	
	private fun testCompiler(compiler : File, commonFlags : Array<String>, arch : Architecture, testFile : File) : Boolean
	{
		val tmp = File.createTempFile("test", ".out")
		try
		{
			val builder = ProcessBuilder(compiler.absolutePath, *commonFlags, *arch.flags, "-o", tmp.absolutePath, testFile.absolutePath)
			logger.debug("$" + builder.command().joinToString(" "))
			//builder.redirectError(INHERIT)
			val process = builder.start()
			return (process.waitFor() == 0 && tmp.exists())
		}
		catch (ex : IOException)
		{
			ex.printStackTrace()
			return false
		}
		finally
		{
			tmp.delete()
		}
	}
	
	@Throws(IOException::class)
	fun searchCompilers(cflags : Array<String>, cxxflags : Array<String>, ldflags : Array<String>, wldflags : Array<String>,
						cacheFile : File?, required: Array<Arch>?)
	{
		val props = Properties()
		if (cacheFile?.exists() ?: false)
		{
			logger.info("Loading compilers cache ${cacheFile?.absolutePath}")
			props.load(FileReader(cacheFile))
			
			for (os in OS.values())
			{
				for (arch in Architecture.values())
				{
					val a = Arch(os, arch)
					if (props.containsKey("$a") && java.lang.Boolean.valueOf(props["$a"] as String))
					{
						logger.info("Loading compiler for $a from cache")
						val compilerArch = Arch(OS.valueOf(props["$a.os"] as String), Architecture.valueOf(props["$a.arch"] as String))
						compilers[a] = Compiler(
								compilerArch, a,
								File(props["$a.cc"] as String),
								File(props["$a.cxx"] as String),
								cflags,
								cxxflags,
								if (compilerArch.os == OS.WINDOWS) wldflags else ldflags
						)
					}
				}
			}
			
			if (required != null)
			{
				val missing = required.filter { !compilers.containsKey(it) }
				if (missing.isEmpty())
					return
				logger.info("Missing compilers (not in cache): " + missing.joinToString(", "))
			}
		}
		
		// extract the test files that should be compiled
		val cTest = File.createTempFile("test", ".c")
		cTest.deleteOnExit()
		IOUtils.copy(javaClass.getResourceAsStream("test.c"), FileOutputStream(cTest))
		val cxxTest = File.createTempFile("test", ".cpp")
		cxxTest.deleteOnExit()
		IOUtils.copy(javaClass.getResourceAsStream("test.cpp"), FileOutputStream(cxxTest))
		
		
		// search the entire path
		val dirs = System.getenv("PATH")
				.split(System.getProperty("path.separator"))
				.map { File(it) }
				.filter { it.exists() && it.isDirectory }
		for (dir in dirs)
		{
			for (file in dir.listFiles({ file ->
				file.isFile && file.canExecute() && (file.name == "clang" || file.name.endsWith("gcc")) }))
			{
				val cc  = file
				val cxx = if (cc.name.endsWith("gcc")) File(cc.parent, cc.name.replaceAfterLast('-', "g++"))
				else File(cc.parent, "clang++")
				if (!cxx.exists() || !cxx.canExecute())
					continue
				
				// run <command> -dumpmachine
				val dumpmachine = runAndOutput(cc.absolutePath, "-dumpmachine") ?: continue
				
				var arch : Architecture? = null
				var os : OS? = null
				
				if (dumpmachine.contains("linux"))
					os = OS.LINUX // might be overridden by android
				if (dumpmachine.contains("android"))
					os = OS.ANDROID
				if (dumpmachine.contains("w32") || dumpmachine.contains("w64") || dumpmachine.contains("mingw"))
					os = OS.WINDOWS
				
				if (dumpmachine.contains("i686") || dumpmachine.contains("x86"))
					arch = Architecture.X86 // might be overridden by x86_64
				if (dumpmachine.contains("x86_64") || dumpmachine.contains("amd64"))
					arch = Architecture.X86_64
				if (dumpmachine.contains("arm"))
					arch = Architecture.ARMv7
				if (dumpmachine.contains("arm64") || dumpmachine.contains("armv8") || dumpmachine.contains("aarch64"))
					arch = Architecture.AARCH64
				
				if (arch == null || os == null)
					continue
				
				// test the compiler
				if (!testCompiler(cc, cflags, arch, cTest) ||
						!testCompiler(cxx, cxxflags, arch, cxxTest))
					continue
				
				val a = Arch(os, arch)
				val ldFlags = if (os == OS.WINDOWS) wldflags else ldflags
				logger.info("Found compiler for $a: ${cc.absolutePath}")
				
				if (compilers.containsKey(a))
				{
					val compiler = compilers[a]!!
					if (compiler.native == a && cc.name != "clang")
						continue
				}
				compilers[a] = Compiler(a, a, cc, cxx, cflags, cxxflags, ldFlags)
				
				if (arch == Architecture.X86_64)
				{
					val a32 = Arch(os, Architecture.X86)
					if ((!compilers.containsKey(a32) || (compilers[a32]!!.native != a32 && cc.name == "clang")) &&
							testCompiler(cc, cflags, Architecture.X86, cTest) &&
							testCompiler(cxx, cxxflags, Architecture.X86, cTest))
						compilers[a32] = Compiler(a, a32, cc, cxx, cflags, cxxflags, ldFlags)
				}
			}
		}
		
		if (cacheFile != null)
		{
			for ((a,compiler) in compilers)
			{
				props["$a"] = true.toString()
				props["$a.os"]   = compiler.native.os.name
				props["$a.arch"] = compiler.native.arch.name
				props["$a.cc"]   = compiler.cc.absolutePath
				props["$a.cxx"]  = compiler.cxx.absolutePath
			}
			props.store(FileWriter(cacheFile), null)
		}
	}
}
