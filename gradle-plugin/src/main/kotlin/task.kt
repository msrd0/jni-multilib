/*
 gradle-jni-multilib
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl-3.0>.
*/

package msrd0.jni.jni_multilib.gradle

import msrd0.jni.jni_multilib.Arch
import msrd0.jni.jni_multilib.Architecture.*
import msrd0.jni.jni_multilib.OS.*
import msrd0.jni.jni_multilib.gradle.Compilers.compilers
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.*
import org.slf4j.*
import java.io.File
import java.lang.System.*

val logger : Logger = LoggerFactory.getLogger(JniMultilibTask::class.java)

/**
 * The `jni` task of this plugin.
 */
open class JniMultilibTask : DefaultTask()
{
	/** The source directories to search for c/c++ files. */
	@get:[InputFiles]
	var sourceDirs = arrayOf("src/main/jni")
	
	/** Some additional source files to include. */
	@get:[InputFiles]
	var sourceFiles = arrayOf<String>()
	
	/** A list of directories to search for c/c++ header files only. */
	@get:[InputFiles]
	var includePath = arrayOf<String>()
	
	/** Specifies whether or not to look recursively for source files. */
	var recursive = true
	
	/** The directory where generated object and library files go. */
	@get:[OutputDirectory]
	var outputDir = File(project.buildDir, "jni")
	
	/** The resources output directory to put the final libraries. */
	@get:[OutputDirectory]
	var resourcesDir = File(project.buildDir, "resources/main")
	
	/** The name of the library. */
	var libraryName = "jni"
	
	/** The architectures to compile the jni. */
	var arch = arrayOf(
			Arch(LINUX, X86),
			Arch(LINUX, X86_64),
			Arch(LINUX, ARMv7),
			Arch(WINDOWS, X86),
			Arch(WINDOWS, X86_64)
	)
	
	/** All file endings used to recognize header files. */
	var headerSuffixes = arrayOf(".h", ".hh", ".hpp", ".hxx")
	
	/** All file endings used to recognize C source files. */
	var cSuffixes = arrayOf(".c")
	
	/** All file endings used to recognize C++ source files. */
	var cxxSuffixes = arrayOf(".cc", ".cpp", ".cxx")
	
	/** Custom flags to the C and C++ compiler. */
	var customFlags = arrayOf<String>()
	
	/** Custom flags only to the C compiler. */
	var customCFlags = arrayOf<String>()
	
	/** Custom flags only to the C++ compiler. */
	var customCxxFlags = arrayOf<String>()
	
	/** Custom flags to the linker. */
	var customLdFlags = arrayOf<String>()
	
	/** Common flags to the C compiler. It is recommended to use `customCFlags` instead. */
	var commonCFlags = Compilers.commonCFlags
	
	/** Common flags to the C++ compiler. It is recommended to use `customCxxFlags` instead. */
	var commonCxxFlags = Compilers.commonCxxFlags
	
	/** Common flags to the Linker. It is recommended to use `customLdFlags` instead. */
	var commonLdFlags = Compilers.commonLdFlags
	
	/** Windows flags to the Linker. It is recommended to use `customLdFlags` instead. */
	var windowsLdFlags = Compilers.windowsLdFlags
	
	private fun ensureExists(dir : File)
	{
		if (!dir.exists())
			if (!dir.mkdirs())
				throw RuntimeException("Cannot create directory $dir")
	}
	
	@TaskAction
	fun action()
	{
		if (sourceDirs.isEmpty() && sourceFiles.isEmpty())
		{
			logger.warn("Skipping tasks due to empty source dirs and empty source files")
			return;
		}
		
		if (!File(getenv("JAVA_HOME") + "/include/jni.h").exists())
		{
			logger.error("Could not locate file jni.h")
			logger.error("Please make sure that your JAVA_HOME environment variable points to a JDK")
			throw RuntimeException("Could not locate file jni.h")
		}
		
		ensureExists(outputDir)
		ensureExists(resourcesDir)
		
		val cflags = arrayOf(*commonCFlags, *customFlags, *customCFlags)
		val cxxflags = arrayOf(*commonCxxFlags, *customFlags, *customCxxFlags)
		val ldflags = arrayOf(*commonLdFlags, *customLdFlags)
		val wldflags = arrayOf(*windowsLdFlags, *customLdFlags)
		
		val sources = Sources(headerSuffixes, cSuffixes, cxxSuffixes)
		for (dir in sourceDirs)
			sources.addDir(File(dir), recursive)
		for (file in sourceFiles)
			sources.addFile(File(file))
		
		val incPath = listOf(*sourceDirs, *includePath,
						getenv("JAVA_HOME") + "/include/",
						getenv("JAVA_HOME") + "/include/linux/")
				.map { File(it) }
		val libs : List<String> = emptyList()
		
		println("headers: ${sources.headers.map { it.absolutePath }}")
		println("c src:   ${sources.c.map { it.absolutePath }}")
		println("cxx src: ${sources.cxx.map { it.absolutePath }}")
		
		Compilers.searchCompilers(cflags, cxxflags, ldflags, wldflags, File(outputDir, "compilers.ini"), arch)
		
		for (a in arch)
		{
			if (!compilers.containsKey(a))
			{
				logger.warn("Skipping $a as no compiler was found")
				continue
			}
			val compiler = compilers[a]!!
			
			val out = File(outputDir, "${a.arch.s}-${a.os.s}")
			val res = File(resourcesDir, "lib-${a.arch.s}-${a.os.s}")
			val so = File(res, a.os.soPrefix + libraryName + a.os.soSuffix)
			
			ensureExists(out)
			ensureExists(res)
			
			val objs = ArrayList<File>()
			for (file in sources.cxx)
				objs.add(compiler.compile(file, true, incPath, out))
			for (file in sources.c)
				objs.add(compiler.compile(file, false, incPath, out))
			
			compiler.link(objs, sources.cxx.isNotEmpty(), libs, so)
			
			logger.info("Compiled ${so.absolutePath} for $a")
		}
	}
}
