/*
 gradle-jni-multilib
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl-3.0>.
*/

package msrd0.jni.jni_multilib.gradle

import msrd0.jni.jni_multilib.Arch
import java.io.File
import java.lang.ProcessBuilder.Redirect.*
import java.nio.charset.StandardCharsets.*
import java.security.MessageDigest
import java.util.*
import kotlin.collections.ArrayList

class Compiler(
		val native : Arch,
		val production : Arch,
		val cc : File,
		val cxx : File,
		var cflags : Array<String> = emptyArray(),
		var cxxflags : Array<String> = emptyArray(),
		var ldflags : Array<String> = emptyArray()
)
{
	companion object
	{
		private val md5md = MessageDigest.getInstance("MD5");
		
		fun md5(src : String) : String
			= Base64.getUrlEncoder().encodeToString(md5md.digest(src.toByteArray(UTF_8))).replace("=", "")
	}
	
	init
	{
		cflags = arrayOf(*cflags, *production.arch.flags)
		cxxflags = arrayOf(*cxxflags, *production.arch.flags)
		ldflags = arrayOf(*ldflags, *production.arch.flags)
	}
	
	fun compile(source : File, isCxx : Boolean, incPath : Collection<File>, outputDir : File) : File
	{
		val obj = File(outputDir, md5(source.absolutePath) + ".o")
		
		val cmd = ArrayList<String>()
		if (isCxx)
		{
			cmd.add(cxx.absolutePath)
			cmd.addAll(cxxflags)
		}
		else
		{
			cmd.add(cc.absolutePath)
			cmd.addAll(cflags)
		}
		for (path in incPath)
			cmd.add("-I${path.absolutePath}")
		cmd.add("-o")
		cmd.add(obj.absolutePath)
		cmd.add("-c")
		cmd.add(source.absolutePath)
		println("$ " + cmd.joinToString(" "))
		
		val pb = ProcessBuilder(cmd)
		pb.redirectError(PIPE)
		pb.redirectOutput(PIPE)
		val process = pb.start()
		if (process.waitFor() != 0)
			throw RuntimeException("The process ${cmd[0]} returned with an exit code != 0")
		
		return obj
	}
	
	fun link(objs : Collection<File>, isCxx : Boolean, libraries : List<String>, so : File)
	{
		val cmd = ArrayList<String>()
		if (isCxx)
			cmd.add(cxx.absolutePath)
		else
			cmd.add(cc.absolutePath)
		cmd.addAll(ldflags)
		cmd.addAll(libraries)
		cmd.add("-o")
		cmd.add(so.absolutePath)
		cmd.addAll(objs.map { it.absolutePath })
		println("$ " + cmd.joinToString(" "))
		
		val pb = ProcessBuilder(cmd)
		pb.redirectError(PIPE)
		pb.redirectOutput(PIPE)
		val process = pb.start()
		if (process.waitFor() != 0)
			throw RuntimeException("The process ${cmd[0]} returned with an exit code != 0")
	}
}
