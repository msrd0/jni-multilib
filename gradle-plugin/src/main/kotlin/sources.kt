/*
 gradle-jni-multilib
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/lgpl-3.0>.
*/

package msrd0.jni.jni_multilib.gradle

import java.io.File

class Sources(
		val headerSuffixes : Array<String>,
		val cSuffixes : Array<String>,
		val cxxSuffixes : Array<String>
)
{
	/** All header files. */
	val headers = ArrayList<File>()
	/** All C source files. */
	val c       = ArrayList<File>()
	/** All C++ source files. */
	val cxx     = ArrayList<File>()
	
	
	fun addDir(dir : File, recursive : Boolean = true)
	{
		if (!dir.exists() || !dir.isDirectory)
			return
		for (file in (dir.listFiles() ?: return))
		{
			if (file.isDirectory)
			{
				if (recursive)
					addDir(file)
				continue
			}
			
			addFile(file)
		}
	}
	
	fun addFile(file : File)
	{
		if (!file.exists() || !file.isFile)
			return;
		
		for (suffix in headerSuffixes)
			if (file.name.endsWith(suffix))
			{
				headers.add(file)
				return;
			}
		for (suffix in cSuffixes)
			if (file.name.endsWith(suffix))
			{
				c.add(file)
				return;
			}
		for (suffix in cxxSuffixes)
			if (file.name.endsWith(suffix))
			{
				cxx.add(file)
				return;
			}
	}
}
