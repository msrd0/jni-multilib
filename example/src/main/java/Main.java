import msrd0.jni.jni_multilib.JniLoader;

import java.io.IOException;

class Main
{
	static native void printHello();
	
	public static void main(String args[]) throws IOException
	{
		JniLoader.loadLibrary("jni");
		
		printHello();
	}
}
